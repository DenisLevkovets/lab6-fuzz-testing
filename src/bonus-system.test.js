import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");


describe('Calculator tests', () => {
    let app;
    console.log("Tests started");


    test('Valid bonus calculation for standart program 1',  (done) => {
        let program = "Standard"
        let amount = 9999;
        assert.equal(calculateBonuses(program, amount),0.05*1);
        done();
    });   
    
    test('Valid bonus calculation for standart program 2',  (done) => {
        let program = "Standard"
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount),0.05*1.5);
        done();
    });  

    test('Valid bonus calculation for standart program 3',  (done) => {
        let program = "Standard"
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount),0.05*2);
        done();
    });  

    test('Valid bonus calculation for standart program 4',  (done) => {
        let program = "Standard"
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount),0.05*2.5);
        done();
    });  

    test('Valid bonus calculation for Premium program 1',  (done) => {
        let program = "Premium"
        let amount = 9999;
        assert.equal(calculateBonuses(program, amount),0.1*1);
        done();
    });   
    
    test('Valid bonus calculation for Premium program 2',  (done) => {
        let program = "Premium"
        let amount = 25000;
        assert.equal(calculateBonuses(program, amount),0.1*1.5);
        done();
    });  

    test('Valid bonus calculation for Premium program 3',  (done) => {
        let program = "Premium"
        let amount = 55000;
        assert.equal(calculateBonuses(program, amount),0.1*2);
        done();
    });  

    test('Valid bonus calculation for Premium program 4',  (done) => {
        let program = "Premium"
        let amount = 105000;
        assert.equal(calculateBonuses(program, amount),0.1*2.5);
        done();
    });  

    test('Valid bonus calculation for Diamond program 1',  (done) => {
        let program = "Diamond"
        let amount = 9999;
        assert.equal(calculateBonuses(program, amount),0.2*1);
        done();
    });   
    
    test('Valid bonus calculation for Diamond program 2',  (done) => {
        let program = "Diamond"
        let amount = 25000;
        assert.equal(calculateBonuses(program, amount),0.2*1.5);
        done();
    });  

    test('Valid bonus calculation for Diamond program 3',  (done) => {
        let program = "Diamond"
        let amount = 55000;
        assert.equal(calculateBonuses(program, amount),0.2*2);
        done();
    });  

    test('Valid bonus calculation for Diamond program 4',  (done) => {
        let program = "Diamond"
        let amount = 105000;
        assert.equal(calculateBonuses(program, amount),0.2*2.5);
        done();
    });  

    test('Valid bonus calculation for Unexpected program',  (done) => {
        let program = "Unexpected"
        let amount = Math.random()*100000;
        assert.equal(calculateBonuses(program, amount),0);
        done();
    });   
    
  

    console.log('Tests Finished');

});
